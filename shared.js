"use strict";
class RoomUsage
    {
        constructor(array)
        {
            this._room = {
                roomNumber: array[0],
                buildingAddress: array[1],
                lightsOn: array[2],
                heatingCoolingOn : array[3],
                computersUsed: array[4],
                computersTotal: array[5],
                seatsUsed: array[6],
                seatsTotal: array[7],
                dateChecked: array[8]
            }
        }
        
        get roomDetails()
        {
            return this._room;
        }
        
    }